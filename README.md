# Asynchronous JavaScript

## Sprint 1 | setTimeout, setInterval

### Use setTimeout

**1. Write the next conversation with a delayed 1s on each sentence**.

+ Hi, my name is L.
+ Nice to meet you, L.
+ What’s is your name?
+ I don’t have one.

#### Expected

+ *wait 1s*
+ Hi, my name is L.
+ *wait 1s*
+ Nice to meet you, L.
+ *wait 1s*
+ What’s is your name?
+ *wait 1s*
+ I don’t have one.

### Use setInterval

**2. Write a Countdown Timer (10 -> 1)**

#### Expected

```js
10
9
8
7
6
5
4
3
2
1
```

*HINT: You need to use clearInterval to stop the timer.*

## Sprint 2 | Promises

### isNumber

+ Write a `isNumber` function that returns a Promise.
+ It should validate if the param is a number.

#### Expected

**Return “Yes, it’s a number.”**

```js
isNumber(2)
  .then(response => console.log(response))
  .catch(err => console.log(err));
```

**Return “Wrong. Not a number.”**

```js
isNumber("2")
  .then(response => console.log(response))
  .catch(err => console.log(err));
```

### addTwo

+ Write a function that returns a Promise.
+ It should return the value plus two.

#### Expected

```js
addTwo(10)
  .then(twelve => console.log(twelve))
  .catch(err => console.log(err));
```

### delay

Write a function that returns the code execution delayed by the time received in the function.

#### Expected

```js
// It should run after 1s
delay(1)
  .then(() => console.log("Hello"));
```

```js
// It should run after 2s
delay(2)
  .then(() => console.log("Hello Again"));
```

### Refactor

Using your `delay` function, refactor the exercise 1.

## Sprint 3 | Requests

### XMLHttpRequest + Promise (Own fetch)

Build your own fetch. You need to use `XMLHttpRequest` and Promises.

#### Expected

```js
get(URL)
  .then(response => console.log(response))
  .catch(err => console.log(err));
```

## Sprint 4 | Fetch

### Fetch

Refactor the above exercise with `fetch`.

### Implementing `fetch` in the real world

## Sprint 5 | Async Await

Use `async/await` in the real world example.

## Sprint 6 | React

Refactor the Application to React.
